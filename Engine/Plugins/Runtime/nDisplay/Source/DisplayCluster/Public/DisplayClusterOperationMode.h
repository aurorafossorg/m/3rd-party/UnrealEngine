// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once


/**
 * Display cluster operation mode
 */
enum class EDisplayClusterOperationMode
{
	Cluster,
	Standalone,
	Editor,
	Disabled
};
